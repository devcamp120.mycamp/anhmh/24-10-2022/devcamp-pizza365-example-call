//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");
const path = require("path");

//Khởi tạo app express
const app = express();

//Khai báo cổng của project 
const port = 8000;

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/example-call", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/example-call.html"))
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})